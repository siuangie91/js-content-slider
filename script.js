//$(document).ready(function() {
    
//set options
var speed = 500; //fade speed
var autoswitch = true; //auto slide
var autoswitchSpeed = 5000; //auto slider speed

//switch to next slide functionality
function nextSlide() {
    $('.active').removeClass('active').addClass('old-active');
    
    if($('.old-active').is(':last-child')) {
        $('.slide').first().addClass('active');
    } else {
        $('.old-active').next().addClass('active');    
    }
    
    $('.old-active').removeClass('old-active');
    
    $('.slide').fadeOut(speed);
    
    $('.active').fadeIn(speed);
}

//switch to prev slide
function prevSlide() {
    $('.active').removeClass('active').addClass('old-active');
    
    if($('.old-active').is(':first-child')) {
        $('.slide').last().addClass('active');
    } else {
        $('.old-active').prev().addClass('active');    
    }
    
    $('.old-active').removeClass('old-active');
    
    $('.slide').fadeOut(speed);
    
    $('.active').fadeIn(speed);
}


//add initial active class 
$('.slide').first().addClass('active');
    
//hide all slides
$('.slide').hide();

//show first slide
$('.active').show();

//next button
$('#next').click(nextSlide);

//prev button
$('#prev').click(prevSlide);

//auto slide
if(autoswitch) {
    setInterval(nextSlide, autoswitchSpeed);
}



//}); //end document.ready











